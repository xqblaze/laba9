#include <iostream>
#include <iomanip>
#include <string.h>

using namespace std;

#include "osadki_opisanie.h"
#include "file_reader.h"
#include "constants.h"
#include "processing.h"

void poisk1(osadki_opisanie* subscriptions[], int size);
void poisk2(osadki_opisanie* subscriptions[], int size);

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �9. GIT\n";
    cout << "������� �3. ������\n";
    cout << "�����: ������ ������\n";
    cout << "������ : 14" << endl;
    osadki_opisanie* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("C:\\Users\\PC\\Desktop\\lab9\\git\\laba9\\labaaaa9\\labaaaa9\\data.txt", subscriptions, size);
        for (int i = 0; i < size; i++)
        {
            /*******����� ����*******/
            cout << "����.......:" << endl;
            cout << setw(2) << setfill('0') << subscriptions[i]->date.day << '-';
            cout << setw(2) << setfill('0') << subscriptions[i]->date.month << ' ' << endl;
            /*******����� ���������� �������*******/
            cout << setw(4) << subscriptions[i]->kol << " mm" << endl;
            /*******��� �������*******/
            cout << "��� �������....:" << endl;
            cout << subscriptions[i]->title << '\n';
            cout << '\n';
        }
        int a;
        cout << "������� 1 ��� ������ �� �������� �����, 2 ��� ������ �� �������� �� ���������� ������� ������ 15 :" << endl;
        cin >> a;
        cout << "��������� ������ :\n" << endl;
        switch (a) {
        case 1:
            poisk1(subscriptions, size);
            break;
        case 2:
            poisk2(subscriptions, size);
            break;
        }
        int b;
        cout << "������� ����� ��� ������ ������� ���-�� ������� (10,11 ��� 12): " << endl;
        cin >> b;
        cout << process(subscriptions, size, b) << endl;

        /*poisk1(subscriptions, size);*/
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}

void poisk1(osadki_opisanie* subscriptions[], int size) {
    for (int i = 0; i < size; i++) {
        if (strcmp(subscriptions[i]->title, "�����") == 0) {
            /*******����� ����*******/
            cout << "����.......:" << endl;
            cout << setw(2) << setfill('0') << subscriptions[i]->date.day << '-';
            cout << setw(2) << setfill('0') << subscriptions[i]->date.month << ' ' << endl;
            /*******����� ���������� �������*******/
            cout << setw(4) << subscriptions[i]->kol << " mm" << endl;
            /*******��� �������*******/
            cout << "��� �������....:" << endl;
            cout << subscriptions[i]->title << '\n';
            cout << '\n';
        }
    }
}

void poisk2(osadki_opisanie* subscriptions[], int size) {
    for (int i = 0; i < size; i++) {
        if (subscriptions[i]->kol <= 15) {
            /*******����� ����*******/
            cout << "����.......:" << endl;
            cout << setw(2) << setfill('0') << subscriptions[i]->date.day << '-';
            cout << setw(2) << setfill('0') << subscriptions[i]->date.month << ' ' << endl;
            /*******����� ���������� �������*******/
            cout << setw(4) << subscriptions[i]->kol << " mm" << endl;
            /*******��� �������*******/
            cout << "��� �������....:" << endl;
            cout << subscriptions[i]->title << '\n';
            cout << '\n';
        }
    }
}