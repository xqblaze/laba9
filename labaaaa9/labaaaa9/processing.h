#ifndef PROCESSING_H
#define PROCESSING_H

#include "osadki_opisanie.h"


int process(osadki_opisanie* array[], int size, int b)
{
	int s = 0;
	for (int i = 0; i < size; i++) {
		if (array[i]->date.month == b) {
			s += array[i]->kol;
		}
	}
	return s;
}


#endif
