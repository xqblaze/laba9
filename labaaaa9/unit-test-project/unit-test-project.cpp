#include "pch.h"
#include "CppUnitTest.h"
//#include "processing.h"
#include "../unit-test-project/osadki_opisanie.h"
#include "../unit-test-project/processing.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestproject
{
	osadki_opisanie* osadki(int day, int month, int kol) {
		osadki_opisanie* subscription = new osadki_opisanie;
		subscription->date.day = day;
		subscription->date.month = month;
		subscription->kol = kol;
		return subscription;
	}

	void delete_subscription(osadki_opisanie* array[], int size)
	{
		for (int i = 0; i < size; i++)
		{
			delete array[i];
		}
	}

	TEST_CLASS(unittestproject)
{
	public:

		TEST_METHOD(TestMethod1)
		{
			osadki_opisanie* subscriptions[6];
			subscriptions[0] = osadki(02, 10, 10);
			subscriptions[1] = osadki(04, 10, 21);
			subscriptions[2] = osadki(06, 10, 15);
			subscriptions[3] = osadki(10, 10, 14);
			subscriptions[4] = osadki(11, 10, 19);
			subscriptions[5] = osadki(23, 10, 52);
			Assert::AreEqual(131, process(subscriptions, 6, 10));
			delete_subscription(subscriptions, 6);
		}
		TEST_METHOD(TestMethod2)
		{
			osadki_opisanie* subscriptions[6];
			subscriptions[0] = osadki(01, 11, 10);
			subscriptions[1] = osadki(02, 11, 21);
			subscriptions[2] = osadki(03, 11, 22);
			subscriptions[3] = osadki(06, 11, 18);
			subscriptions[4] = osadki(18, 11, 19);
			subscriptions[5] = osadki(25, 11, 36);
			Assert::AreEqual(126, process(subscriptions, 6, 11));
			delete_subscription(subscriptions, 6);
		}
		TEST_METHOD(TestMethod3)
		{
			osadki_opisanie* subscriptions[9];
			subscriptions[0] = osadki(01, 12, 5);
			subscriptions[1] = osadki(02, 12, 2);
			subscriptions[2] = osadki(04, 12, 8);
			subscriptions[3] = osadki(05, 12, 14);
			subscriptions[4] = osadki(06, 12, 11);
			subscriptions[5] = osadki(11, 12, 24);
			subscriptions[6] = osadki(13, 12, 8);
			subscriptions[7] = osadki(15, 12, 20);
			subscriptions[8] = osadki(19, 12, 40);
			Assert::AreEqual(132, process(subscriptions, 9, 12));
			delete_subscription(subscriptions, 9);
		}
	};
}
