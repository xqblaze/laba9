#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H

#include "constants.h"

struct date
{
    int day;
    int month;
};

struct osadki_opisanie
{
    date date;
    int kol;
    char title[MAX_STRING_SIZE];
};

#endif

